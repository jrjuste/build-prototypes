const { src, dest, watch, series } = require('gulp');
//const sass = require('gulp-sass');
const postcss = require('gulp-postcss');
const cssnano = require('cssnano');
const terser = require('gulp-terser');
const browsersync = require('browser-sync').create();
const sass = require('gulp-sass')(require('sass'));

// Sass Task
function scssTask(){
    return src('src/scss/styles.scss', { sourcemaps: true })
        .pipe(sass())
        .pipe(postcss([cssnano()]))
        .pipe(dest('dist', { sourcemaps: '.' }));
  };

  // JavaScript Task
function jsTask(){
    return src('src/app.js', { sourcemaps: true })
      .pipe(terser())
      .pipe(dest('dist', { sourcemaps: '.' }));
  };

  function browsersyncServe(cb){
    browsersync.init({
      server: {
        baseDir: "src",
        index: "index.html"
    }  
    });
    cb();
  };

  function browsersyncReload(cb){
    browsersync.reload();
    cb();
  };

  // Watch Task
function watchTask(){
    watch('*.html', browsersyncReload);
    watch(['src/**/*.scss', 'src/**/*.js'], series(scssTask, jsTask, browsersyncReload));
  }

  // Default Gulp Task
exports.default = series(
    scssTask,
    jsTask,
    browsersyncServe,
    watchTask
  );